package com.epam.hibernategymtask.repository;

import com.epam.hibernategymtask.config.HibernateConfig;
import com.epam.hibernategymtask.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {HibernateConfig.class})
@Sql({"classpath:schema.sql", "classpath:data.sql"})
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    void save() {
        var user = User.builder().firstname("Bob").lastname("Frost").username("Bob.Frost").password("password10").isActive(true).build();

        var savedUser = userRepository.save(user);

        Assertions.assertEquals(user.getFirstname(), savedUser.getFirstname());
        Assertions.assertEquals(user.getLastname(), savedUser.getLastname());
        Assertions.assertEquals(user.getUsername(), savedUser.getUsername());
        Assertions.assertEquals(user.getPassword(), savedUser.getPassword());
        Assertions.assertEquals(user.getIsActive(), savedUser.getIsActive());
        Assertions.assertNotNull(savedUser.getId());
    }

    @Test
    void findAll() {
        var size = userRepository.findAll().size();
        Assertions.assertEquals(10, size);
    }

    @Test
    void getUserByUsername() {
        var username = "emily_davis";
        var expectedUser = User.builder()
                .firstname("Emily")
                .lastname("Davis")
                .username("emily_davis")
                .password("em_pass123")
                .isActive(true)
                .build();

        var user = userRepository.getUserByUsername(username).get();
        Assertions.assertEquals(expectedUser.getFirstname(), user.getFirstname());
        Assertions.assertEquals(expectedUser.getLastname(), user.getLastname());
        Assertions.assertEquals(expectedUser.getUsername(), user.getUsername());
        Assertions.assertEquals(expectedUser.getIsActive(), user.getIsActive());
        Assertions.assertEquals(expectedUser.getPassword(), user.getPassword());
    }
}