package com.epam.hibernategymtask.repository;

import com.epam.hibernategymtask.model.Coach;
import com.epam.hibernategymtask.model.User;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class CoachRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Coach save(Coach entity) {
        if (entityManager.contains(entity)) {
            entity = entityManager.merge(entity);
        } else {
            entityManager.persist(entity);
        }

        return entity;
    }

    public void delete(Coach coach) {
        entityManager.remove(coach);
        entityManager.flush();
    }


    public List<Coach> findAll() {
        var criteria = entityManager.getCriteriaBuilder().createQuery(Coach.class);
        criteria.from(Coach.class);

        return entityManager.createQuery(criteria)
                .getResultList();
    }


    public Optional<Coach> getCoachByUsername(String username) {
        var criteriaBuilder = entityManager.getCriteriaBuilder();
        var criteriaQuery = criteriaBuilder.createQuery(Coach.class);

        Root<Coach> traineeRoot = criteriaQuery.from(Coach.class);
        Join<Coach, User> userJoin = traineeRoot.join("user"); // Assuming "user" is the field in Coach class.

        criteriaQuery.select(traineeRoot)
                .where(criteriaBuilder.equal(userJoin.get("username"), username));

        TypedQuery<Coach> query = entityManager.createQuery(criteriaQuery);

        // Assuming there should be only one Trainee for a given username. If not, consider handling multiple results.
        return query.getResultList().stream().findFirst();
    }
}
