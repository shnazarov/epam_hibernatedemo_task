package com.epam.hibernategymtask.dto;

public record RegisterResponse(
        String username,
        String password
) {
}
