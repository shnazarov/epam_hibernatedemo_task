package com.epam.hibernategymtask;

import com.epam.hibernategymtask.config.HibernateConfig;
import com.epam.hibernategymtask.model.Trainee;
import com.epam.hibernategymtask.repository.TraineeRepository;
import com.epam.hibernategymtask.repository.UserRepository;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDate;


public class ApplicationRunner {
    public static void main(String[] args) {
        var context = new AnnotationConfigApplicationContext(HibernateConfig.class);


        var userRepository = context.getBean(UserRepository.class);
        var traineeRepository = context.getBean(TraineeRepository.class);

        //var session = context.getBean(Session.class);

        // session.beginTransaction();

//        var user = User.builder()
//                .firstname("Shohruh2")
//                .lastname("Nazarov2")
//                .username("nazarik89992")
//                .password("12345")
//                .isActive(true)
//                .build();

//        var user = userRepository.getUserByUsername("nazarik8999").get();

//        var trainee = Trainee.builder()
//                .user(user)
//                .dateOfBirth(LocalDate.of(2021, 1, 1))
//                .build();

        //System.out.println(traineeRepository.save(trainee));

        //System.out.println(traineeRepository.getTraineeByUsername("nazarik8999"));

//        System.out.println(userRepository.getUserByUsername("nazarik899").orElseThrow(
//                () -> new DataNotFoundException("User", "usernmae: nazarik899")));

        //session.getTransaction().commit();

    }
}
