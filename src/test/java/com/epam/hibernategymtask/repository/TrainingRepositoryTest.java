package com.epam.hibernategymtask.repository;

import com.epam.hibernategymtask.config.HibernateConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {HibernateConfig.class})
@Sql({"classpath:schema.sql", "classpath:data.sql"})
class TrainingRepositoryTest {

    @Autowired
    private TrainingRepository trainingRepository;


    @Test
    void findActiveTrainersNotAssignedOnTrainee() {
        var trainerList = trainingRepository.findActiveCoachesNotAssignedOnTrainee("emily_davis");
        Assertions.assertEquals(4, trainerList.size());
        Assertions.assertEquals("chris_evans", trainerList.get(0).getUser().getUsername());
    }

    @Test
    void findTrainingsOfTrainee_singleUsername() {
        var trainingList = trainingRepository.findTrainingsOfTrainee(
                "alex_wong",
                null, null, null, null
        );
        Assertions.assertEquals(1, trainingList.size());
    }

    @Test
    void findTrainingsOfTrainee_username_trainerUsername() {
        var trainingList = trainingRepository.findTrainingsOfTrainee(
                "emily_davis",
                null, null, "sophia_miller", null
        );
        Assertions.assertEquals(1, trainingList.size());
        Assertions.assertEquals(5, trainingList.get(0).getId());
    }

    @Test
    void findTrainingsOfCoach() {
        var trainingList = trainingRepository.findTrainingsOfCoach(
                "daniel_lee",
                null, null, null
        );
        Assertions.assertEquals(1, trainingList.size());
    }
}