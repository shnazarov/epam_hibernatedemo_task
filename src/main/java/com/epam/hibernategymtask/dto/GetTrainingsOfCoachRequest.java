package com.epam.hibernategymtask.dto;

import java.time.LocalDateTime;

public record GetTrainingsOfCoachRequest(
        String username,
        LocalDateTime periodFrom,
        LocalDateTime periodTo,
        String traineeUsername
) {
}

