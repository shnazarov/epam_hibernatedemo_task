package com.epam.hibernategymtask.dto;

import java.time.LocalDate;

public record UpdateTraineeProfileRequest(
        String username,
        String firstname,
        String lastname,
        LocalDate dateOfBirth,
        String address
) {
    public UpdateTraineeProfileRequest(String username, String firstname, String lastname, LocalDate dateOfBirth, String address) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }

    public UpdateTraineeProfileRequest(String username, String firstname, String lastname, boolean isActive) {
        this(username, firstname, lastname, null, null);
    }
}
