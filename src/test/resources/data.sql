-- Insert more data into users table
INSERT INTO users(isActive, firstname, lastname, password, username)
VALUES (true, 'Emily', 'Davis', 'em_pass123', 'emily_davis'),
       (true, 'Chris', 'Evans', 'chrisevanspass', 'chris_evans'),
       (true, 'Lily', 'Johnson', 'lily123', 'lily_johnson'),
       (true, 'Alex', 'Wong', 'alex_pass', 'alex_wong'),
       (true, 'Sophia', 'Miller', 'sophia456', 'sophia_miller'),
       (true, 'Daniel', 'Lee', 'daniel_pass', 'daniel_lee'),
       (true, 'Ella', 'Brown', 'ella_pass', 'ella_brown'),
       (true, 'Oscar', 'Clark', 'oscar123', 'oscar_clark'),
       (true, 'Grace', 'Taylor', 'grace_pass', 'grace_taylor'),
       (true, 'Ryan', 'Smith', 'ryan456', 'ryan_smith');

INSERT INTO training_type (trainingTypeName)
VALUES ('BODYBUILDING'),
       ('SWIMMING'),
       ('FITNESS'),
       ('WORKOUT'),
       ('BOXING'),
       ('TAEKWONDO'),
       ('KARATE'),
       ('JUJITSU');

-- Insert more data into coach table
INSERT INTO coach (user_id, training_type_id)
VALUES (2, 2), -- Chris Evans is a coach for SWIMMING
       (6, 4), -- Daniel Lee is a coach for WORKOUT
       (9, 6), -- Grace Taylor is a coach for TAEKWONDO
       (3, 1), -- Lily Johnson is a coach for BODYBUILDING
       (5, 5); -- Sophia Miller is a coach for BOXING


-- Insert more data into trainee table
INSERT INTO trainee (dateOfBirth, user_id)
VALUES ('1992-05-28', 4), -- Alex Wong is a trainee
       ('1988-09-15', 7), -- Ella Brown is a trainee
       ('1997-11-03', 10), -- Ryan Smith is a trainee
       ('1990-03-14', 8), -- Oscar Clark is a trainee
       ('1995-08-22', 1); -- Emily Lee is a trainee


-- Insert more data into training table
INSERT INTO training (durationInMinutes, trainingDate, coach_id, trainee_id, training_type_id, trainingName)
VALUES (45, '2023-04-15', 1, 1, 2, 'Swimming Session for Alex'), -- Chris Evans coaching Alex in SWIMMING
       (60, '2023-05-20', 2, 2, 4, 'Workout Session for Ella'),    -- Daniel Lee coaching Ella in WORKOUT
       (45, '2023-06-10', 3, 3, 6, 'Taekwondo Session for Ryan'), -- Grace Taylor coaching Ryan in TAEKWONDO
       (30, '2023-07-01', 4, 4, 1, 'Bodybuilding Session for Lily'), -- Lily Johnson coaching Oscar in BODYBUILDING
       (45, '2023-08-15', 5, 5, 5, 'Boxing Session for Sophia'); -- Sophia Miller coaching Emily in BOXING
