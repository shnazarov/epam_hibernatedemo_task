package com.epam.hibernategymtask.util;

import java.security.SecureRandom;

public final class ProfileGeneratorUtil {

    private ProfileGeneratorUtil() {
    }

    public static String passwordGenerator() {
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+";
        StringBuilder password = new StringBuilder();
        SecureRandom random = new SecureRandom();

        for (int i = 0; i < 10; i++) {
            int randomIndex = random.nextInt(chars.length());
            password.append(chars.charAt(randomIndex));
        }
        return password.toString();
    }


    public static String usernameGeneratorWithSerialNumber(String username) {
        return username + new SecureRandom().nextInt(1000);
    }

    public static String usernameGenerator(String firstname, String lastname) {
        return firstname + "." + lastname;
    }
}
