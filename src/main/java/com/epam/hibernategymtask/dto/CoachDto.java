package com.epam.hibernategymtask.dto;

public record CoachDto(
        String username,
        String firstname,
        String lastname,
        TrainingTypeDto specialization
) {
}
