package com.epam.hibernategymtask.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "user")
@Builder
@Entity
@Table(name = "coach")
public class Coach {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "training_type_id", nullable = false)
    private TrainingType specialization;

    @Builder.Default
    @OneToMany(mappedBy = "coach", cascade = CascadeType.ALL)
    private Set<Training> trainings = new HashSet<>();
}
