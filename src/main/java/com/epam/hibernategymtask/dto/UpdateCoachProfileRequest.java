package com.epam.hibernategymtask.dto;

import java.util.List;

public record UpdateCoachProfileRequest(
        String username,
        String firstname,
        String lastname,
        TrainingTypeDto specialization
) {
}
