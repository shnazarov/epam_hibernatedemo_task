package com.epam.hibernategymtask.dto;


public record RegisterCoachRequest(
        String firstname,
        String lastname,
        TrainingTypeDto specialization
) {

}
