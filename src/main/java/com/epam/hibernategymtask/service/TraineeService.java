package com.epam.hibernategymtask.service;

import com.epam.hibernategymtask.dto.ActivateDeactivate;
import com.epam.hibernategymtask.dto.RegisterResponse;
import com.epam.hibernategymtask.dto.RegisterTraineeRequest;
import com.epam.hibernategymtask.dto.UpdateTraineeProfileRequest;
import com.epam.hibernategymtask.exception.DataNotFoundException;
import com.epam.hibernategymtask.model.Trainee;
import com.epam.hibernategymtask.model.User;
import com.epam.hibernategymtask.repository.TraineeRepository;
import com.epam.hibernategymtask.repository.UserRepository;
import com.epam.hibernategymtask.util.ProfileGeneratorUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class TraineeService {

    private TraineeRepository traineeRepository;
    private UserRepository userRepository;

    public RegisterResponse registerTrainee(RegisterTraineeRequest request) {

        var username = ProfileGeneratorUtil.usernameGenerator(request.firstname(), request.lastname());

        while (userRepository.getUserByUsername(username).isPresent()) {
            username = ProfileGeneratorUtil.usernameGeneratorWithSerialNumber(username);
        }

        var password = ProfileGeneratorUtil.passwordGenerator();
        var user = User.builder()
                .firstname(request.firstname())
                .lastname(request.lastname())
                .username(username)
                .password(password)
                .isActive(true)
                .build();


        var savedUser = userRepository.save(user);

        var trainee = Trainee.builder()
                .user(savedUser)
                .dateOfBirth(request.dateOfBirth())
                .build();
        var savedTrainee = traineeRepository.save(trainee);


        return new RegisterResponse(savedTrainee.getUser().getUsername(), savedTrainee.getUser().getPassword());
    }

    public Trainee updateTraineeProfile(UpdateTraineeProfileRequest request) {
        var username = request.username();

        var trainee = traineeRepository.getTraineeByUsername(username)
                .orElseThrow(() -> new DataNotFoundException("Trainee", username));

        trainee.getUser().setFirstname(request.firstname());
        trainee.getUser().setLastname(request.lastname());
        trainee.setDateOfBirth(request.dateOfBirth());

        return traineeRepository.save(trainee);
    }

    public void deleteTrainee(String username) {
        var traineeOptional = traineeRepository.getTraineeByUsername(username);
        traineeOptional.ifPresent(traineeRepository::delete);
    }

    public void activateDeactivateTrainee(ActivateDeactivate request) {
        var trainee = traineeRepository.getTraineeByUsername(request.username())
                .orElseThrow(() -> new DataNotFoundException("Coach", request.username()));

        trainee.getUser().setIsActive(request.isActive());
    }
}
