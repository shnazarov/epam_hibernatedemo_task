package com.epam.hibernategymtask.repository;

import com.epam.hibernategymtask.config.HibernateConfig;
import com.epam.hibernategymtask.model.Coach;
import com.epam.hibernategymtask.model.TrainingType;
import com.epam.hibernategymtask.model.TrainingTypeName;
import com.epam.hibernategymtask.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {HibernateConfig.class})
@Sql({"classpath:schema.sql", "classpath:data.sql"})
@Transactional
class CoachRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CoachRepository coachRepository;

    @Autowired
    private TrainingTypeRepository trainingTypeRepository;

    @Test
    void save() {
        var user = User.builder()
                .firstname("Alex")
                .lastname("Chuck")
                .username("Alex.Chuck")
                .password("alex12345")
                .isActive(true)
                .build();

        user = userRepository.save(user);

        var specialization = TrainingType.builder()
                .trainingTypeName(TrainingTypeName.BOXING)
                .build();

        specialization = trainingTypeRepository.save(specialization);

        var coach = Coach.builder()
                .user(user)
                .specialization(specialization)
                .build();


        var savedCoach = coachRepository.save(coach);

        Assertions.assertEquals(user.getFirstname(), savedCoach.getUser().getFirstname());
        Assertions.assertEquals(user.getLastname(), savedCoach.getUser().getLastname());
        Assertions.assertEquals(user.getIsActive(), savedCoach.getUser().getIsActive());
        Assertions.assertEquals(user.getUsername(), savedCoach.getUser().getUsername());
        Assertions.assertEquals(user.getPassword(), savedCoach.getUser().getPassword());

        Assertions.assertEquals(specialization.getId(), savedCoach.getSpecialization().getId());
        Assertions.assertEquals(specialization.getTrainingTypeName(), savedCoach.getSpecialization().getTrainingTypeName());
        Assertions.assertEquals(0, savedCoach.getTrainings().size());
    }

    @Test
    void delete() {
        var coach = coachRepository.getCoachByUsername("chris_evans").get();
        coachRepository.delete(coach);
        var coachesSize = coachRepository.findAll().size();
        Assertions.assertEquals(2, coachesSize);
    }


    @Test
    void findAll() {
        // When
        var coaches = coachRepository.findAll();

        // Then
        Assertions.assertFalse(coaches.isEmpty());
        Assertions.assertEquals(3, coaches.size());
    }

    @Test
    void getCoachByUsername() {
        // Given
        var user = User.builder()
                .firstname("Mike")
                .lastname("Johnson")
                .username("mike.johnson")
                .password("password")
                .isActive(true)
                .build();

        user = userRepository.save(user);

        var trainingType = TrainingType.builder()
                .trainingTypeName(TrainingTypeName.BOXING)
                .build();

        trainingType = trainingTypeRepository.save(trainingType);

        var coach = Coach.builder()
                .user(user)
                .specialization(trainingType)
                .build();

        coachRepository.save(coach);

        // When
        Optional<Coach> foundCoach = coachRepository.getCoachByUsername("mike.johnson");

        // Then
        Assertions.assertTrue(foundCoach.isPresent());
        Assertions.assertEquals("mike.johnson", foundCoach.get().getUser().getUsername());
        Assertions.assertEquals(TrainingTypeName.BOXING, foundCoach.get().getSpecialization().getTrainingTypeName());
    }
}