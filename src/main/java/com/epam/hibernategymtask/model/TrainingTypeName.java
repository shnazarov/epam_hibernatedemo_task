package com.epam.hibernategymtask.model;

public enum TrainingTypeName {
    BODYBUILDING,
    SWIMMING,
    FITNESS,
    WORKOUT,
    BOXING,
    TAEKWONDO,
    KARATE,
    JUJITSU
}
