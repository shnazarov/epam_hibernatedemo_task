package com.epam.hibernategymtask.service;

import com.epam.hibernategymtask.dto.ActivateDeactivate;
import com.epam.hibernategymtask.dto.RegisterCoachRequest;
import com.epam.hibernategymtask.dto.RegisterResponse;
import com.epam.hibernategymtask.dto.UpdateCoachProfileRequest;
import com.epam.hibernategymtask.exception.DataNotFoundException;
import com.epam.hibernategymtask.model.Coach;
import com.epam.hibernategymtask.model.TrainingType;
import com.epam.hibernategymtask.model.User;
import com.epam.hibernategymtask.repository.CoachRepository;
import com.epam.hibernategymtask.repository.UserRepository;
import com.epam.hibernategymtask.util.ProfileGeneratorUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class CoachService {

    private CoachRepository coachRepository;
    private UserRepository userRepository;


    public RegisterResponse registerCoach(RegisterCoachRequest request) {
        var username = ProfileGeneratorUtil.usernameGenerator(request.firstname(), request.lastname());

        while (userRepository.getUserByUsername(username).isPresent()) {
            username = ProfileGeneratorUtil.usernameGeneratorWithSerialNumber(username);
        }

        var password = ProfileGeneratorUtil.passwordGenerator();
        var user = User.builder()
                .firstname(request.firstname())
                .lastname(request.lastname())
                .username(username)
                .password(password)
                .isActive(true)
                .build();


        var savedUser = userRepository.save(user);

        var coach = Coach.builder()
                .user(savedUser)
                .specialization(
                        TrainingType.builder()
                                .id(request.specialization().id())
                                .trainingTypeName(request.specialization().trainingTypeName())
                                .build()
                )
                .build();
        var savedCoach = coachRepository.save(coach);


        return new RegisterResponse(savedCoach.getUser().getUsername(), savedCoach.getUser().getPassword());
    }

    public Coach updateTrainerProfile(UpdateCoachProfileRequest request) {
        var username = request.username();

        var coach = coachRepository.getCoachByUsername(username)
                .orElseThrow(() -> new DataNotFoundException("Trainee", username));

        coach.getUser().setFirstname(request.firstname());
        coach.getUser().setLastname(request.lastname());
        coach.getSpecialization().setTrainingTypeName(request.specialization().trainingTypeName());


        return coachRepository.save(coach);
    }

    public void deleteCoach(String username) {
        var coachOptional = coachRepository.getCoachByUsername(username);
        coachOptional.ifPresent(coach -> coachRepository.delete(coach));
    }

    public void activateDeactivateCoach(ActivateDeactivate request) {
        var coach = coachRepository.getCoachByUsername(request.username())
                .orElseThrow(() -> new DataNotFoundException("Coach", request.username()));

        coach.getUser().setIsActive(request.isActive());
    }

}
