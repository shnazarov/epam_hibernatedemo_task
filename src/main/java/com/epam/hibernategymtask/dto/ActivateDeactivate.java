package com.epam.hibernategymtask.dto;

public record ActivateDeactivate(
        String username,
        Boolean isActive
) {

}
