package com.epam.hibernategymtask.exception;

public class AlreadyExistsException extends RuntimeException {
    public AlreadyExistsException(String dataName, String username) {
        super(String.format("%s with : %s username already exists", dataName, username));
    }
}
