package com.epam.hibernategymtask.dto;

import com.epam.hibernategymtask.model.TrainingTypeName;

public record TrainingTypeDto(
        Long id,
        TrainingTypeName trainingTypeName
) {
}
